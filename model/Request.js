const Base = require("./Base");

module.exports = (sequelize, DataTypes) => {
  return Base(sequelize, DataTypes, "request", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    campaign_id: { type: DataTypes.INTEGER, allowNull: false },
    placement_id: { type: DataTypes.INTEGER, allowNull: false },
  });
};
