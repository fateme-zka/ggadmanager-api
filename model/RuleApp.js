const Base = require("./Base");

module.exports = (sequelize, DataTypes) => {
  return Base(sequelize, DataTypes, "rule_app", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    rule_id: { type: DataTypes.INTEGER, allowNull: false },
    app_id: { type: DataTypes.INTEGER, allowNull: false },
  });
};
