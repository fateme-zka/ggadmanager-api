const Base = require("./Base");

module.exports = (sequelize, DataTypes) => {
  return Base(sequelize, DataTypes, "rule", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    country: { type: DataTypes.STRING, allowNull: false },
    campaign_id: { type: DataTypes.INTEGER, allowNull: false },
  });
};
