const Base = require("./Base");

module.exports = (sequelize, DataTypes) => {
  return Base(sequelize, DataTypes, "impression", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    request_id: { type: DataTypes.INTEGER, allowNull: false },
    app_id: { type: DataTypes.INTEGER, allowNull: false },
  });
};
